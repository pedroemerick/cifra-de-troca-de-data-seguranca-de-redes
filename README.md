## CIFRA DE TROCA DE DATA

Este repositório contém dois programas que permitem cifrar e decifrar um arquivo texto seguindo a Cifra de Troca de Data.

---

### Considerações Gerais:

Os programas que permitem cifrar e decifrar utilizam o alfabeto de letras minúsculas, então a mensagem passada irá ser toda transformada em para letras minúsculas, quaisquer outros caracteres que não sejam do alfabeto tradicional, serão ignorados, tanto na cifragem quanto na decifragem.

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila os dois programas;
* "make cifra" = compila o programa que permite cifrar;
* "make decifra" = compila o programa que permite decifrar;
* "make doc" = gera a documentação dos programas, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para cifrar:
* Compile o programa que permite cifrar, usando o comando "make cifra";
* Execute passando os parâmetros corretos:  "./bin/cifra data mensagem.txt mensagem.sec".
    * data = a data escolhida para cifragem no formato dd/mm/aa
    * mensagem.txt = arquivo que contém a mensagem a ser cifrada
    * mensagem.sec = arquivo em que a mensagem cifrada deve ser armazenada

---

### Para decifrar:
* Compile o programa que permite decifrar, usando o comando "make decifra";
* Execute passando os parâmetros corretos:  "./bin/decifra data mensagem.sec";
    * data = a data escolhida para decifragem no formato dd/mm/aa
    * mensagem.sec = arquivo que contém a mensagem cifrada
* A mensagem decifrada será impressa na saída padrão.


