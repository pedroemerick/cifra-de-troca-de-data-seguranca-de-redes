/**
* @file	    cifra.cpp
* @brief	Arquivo com a função principal do programa, que cifra as mensagens
* @author   Pedro Emerick (p.emerick@live.com)
* @since	02/08/2018
* @date	    14/08/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <cstring>

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <vector>
using std::vector;

#include <algorithm>
using std::find;

#include <cstdlib>
using std::atoi;

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "Erro ao ler mensagem !!!" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
    }

    arquivo.close();

    return mensagem;
}

/**
* @brief Função que cifra uma mensagem de acordo com a data passada
* @param mensagem Mensagem a ser cifrada
* @param data Data para cifrar
*/
string cifrar (string mensagem, string data) 
{
    string msg_cifrada;
    vector <string> alfabeto = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    int tamanho_msg = mensagem.size();
    int aux = 0;
    
    for (int ii = 0; ii < tamanho_msg; ii++) {
        if (mensagem[ii] >= 97 && mensagem[ii] <= 122) {
            string letra (1, mensagem[ii]);
            char num_data[1] = {data[aux]};
            int pos_letra = find(alfabeto.begin(), alfabeto.end(), letra) - alfabeto.begin();

            int pos_nova_letra = pos_letra + atoi(num_data);
            while (pos_nova_letra >= (int) alfabeto.size()) {
                pos_nova_letra -= alfabeto.size();
            }

            string nova_letra = alfabeto[pos_nova_letra];
            msg_cifrada += nova_letra;

            aux++;
            if (aux == (int)data.size()) {
                aux = 0;
            }
        } else {
            msg_cifrada += mensagem[ii];
        }
    }

    return msg_cifrada;
}

/**
* @brief Função que grava a mensagem decifrada em um arquivo
* @param mensagem Mensagem cifrada a ser gravada no arquivo
* @param nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq) 
{
    ofstream arquivo (nome_arq);

    if (!arquivo)
    {
        cerr << "Erro ao gravar mensagem cifrada !!!" << endl;
        exit (1);
    }

    arquivo << mensagem << endl;
    arquivo.close ();
}

/**
* @brief Função principal do programa.
*/
int main (int argc, char* argv[]) 
{    
    // Verifica se são passados os argumentos corretamente
    if (argc != 4) {
        cout << "--> Argumentos invalidos ! Use './prog data texto_claro texto_cifrado'" << endl;
        exit(1);
    }

    // Pega a data passada e tira as barras
    char *data_com_sep = strtok (argv[1], "/");
    string data;
    while (data_com_sep) {
        data += data_com_sep;
        data_com_sep = strtok (NULL, "/");
    }

    // Verifica se o formato da data é dd/mm/aa
    if (data.size() != 6) {
        cout << "--> Digite a data no formato dd/mm/aa !" << endl;
        exit(1);
    }

    // Verifica se foram passados numeros na data
    for (int ii = 0; ii < (int) data.size(); ii++) {
        if (data[ii] < 48 || data[ii] > 57) {
            cout << "--> Passe a data em valor númerico para cifragem !" << endl;
            exit(1);
        }
    }

    string entrada_msg = argv[2];
    string saida_msg = argv[3];

    string mensagem = ler_msg (entrada_msg);

    // Coloca a mensagem toda em letra minuscula
    transform(mensagem.begin(), mensagem.end(), mensagem.begin(), tolower);

    string msg_cifrada = cifrar (mensagem, data);

    gravar_msg (msg_cifrada, saida_msg);

    cout << "--> Mensagem cifrada com sucesso !" << endl;

    return 0;
}