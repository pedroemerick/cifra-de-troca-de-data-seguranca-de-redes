/**
* @file	    decifra.cpp
* @brief	Arquivo com a função principal do programa, que cifra as mensagens
* @author   Pedro Emerick (p.emerick@live.com)
* @since	02/08/2018
* @date	    14/08/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <cstring>

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <vector>
using std::vector;

#include <algorithm>
using std::find;

#include <cstdlib>
using std::atoi;

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "Erro ao ler mensagem !!!" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
    }

    arquivo.close();

    return mensagem;
}

/**
* @brief Função que decifra uma mensagem de acordo com a data passada
* @param mensagem Mensagem a ser decifrada
* @param data Data para decifrar a mensagem
*/
string decifrar_msg (string mensagem, string data)
{
    vector <string> alfabeto = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    string msg_decifrada;
    int tamanho_msg = mensagem.size();
    int aux = 0;

    for (int ii = 0; ii < tamanho_msg; ii++) {
        if (mensagem[ii] >= 97 && mensagem[ii] <= 122) {
            string letra (1, mensagem[ii]);
            char num_data[1] = {data[aux]};
            int pos_letra = find(alfabeto.begin(), alfabeto.end(), letra) - alfabeto.begin();

            int pos_nova_letra = pos_letra - atoi(num_data);
            while (pos_nova_letra < 0) {
                pos_nova_letra += alfabeto.size();
            }

            string nova_letra = alfabeto[pos_nova_letra];
            msg_decifrada += nova_letra;

            aux++;
            if (aux == (int)data.size()) {
                aux = 0;
            }
        } else {
            msg_decifrada += mensagem[ii];
        }
    }

    return msg_decifrada;
}

/**
* @brief Função principal do programa.
*/
int main (int argc, char* argv[]) 
{    
    // Verifica se são passados os argumentos corretamente
    if (argc != 3) {
        cout << "--> Argumentos invalidos ! Use './prog data texto_cifrado'" << endl;
        exit(1);
    }

    char *data_com_sep = strtok (argv[1], "/");
    string data;
    while (data_com_sep) {
        data += data_com_sep;
        data_com_sep = strtok (NULL, "/");
    }

    // Verifica se o formato da data é dd/mm/aa
    if (data.size() != 6) {
        cout << "--> Digite a data no formato dd/mm/aa !" << endl;
        exit(1);
    }

    // Verifica se foram passados numeros na data
    for (int ii = 0; ii < (int) data.size(); ii++) {
        if (data[ii] < 48 || data[ii] > 57) {
            cout << "--> Passe a data em valor númerico para cifragem !" << endl;
            exit(1);
        }
    }

    string entrada_msg = argv[2];

    string mensagem = ler_msg (entrada_msg);

    transform(mensagem.begin(), mensagem.end(), mensagem.begin(), tolower);

    string msg_decifrada = decifrar_msg (mensagem, data);

    cout << "--> Mensagem decifrada com sucesso !" << endl << endl;
    cout << msg_decifrada << endl;

    return 0;
}